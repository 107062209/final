const {ccclass, property} = cc._decorator;

@ccclass
export default class Player extends cc.Component 
{

    private anim = null; //this will use to get animation component

    //private animateState = null; //this will use to record animationState

    private playerSpeed: number = 0;

    private zDown: boolean = false; // key for player to go left

    private xDown: boolean = false; // key for player to go right

    private jDown: boolean = false; // key for player to attack

    private kDown: boolean = false; // key for player to jump

    private onGround: boolean = false;

    private isDead: boolean = false;

    onLoad()
    {
        this.anim = this.getComponent(cc.Animation);

        cc.director.getCollisionManager().enabled = true;

        cc.director.getPhysicsManager().enabled = true;

    }

    start() 
    {
        // add key down and key up event
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);

        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }

    update(dt)
    {
        this.playerMovement(dt);
    }

    onKeyDown(event) 
    {
        switch(event.keyCode) 
        {
            case cc.macro.KEY.z:

                this.zDown = true;

                this.xDown = false;

                break;

            case cc.macro.KEY.x:

                this.xDown = true;

                this.zDown = false;

                break;

            case cc.macro.KEY.j:

                this.jDown = true;

                break;

            case cc.macro.KEY.k:

                this.kDown = true;

                break;
        }
    }

    onKeyUp(event)
    {
        switch(event.keyCode) 
        {
            case cc.macro.KEY.z:

                this.zDown = false;

                break;

            case cc.macro.KEY.x:

                this.xDown = false;

                break;

            case cc.macro.KEY.j:

                this.jDown = false;

                break;

            case cc.macro.KEY.k:

                this.kDown = false;

                break;
        }
    }

    private playerMovement(dt)
    {
        if(this.isDead)
            this.playerSpeed = 0;
        else if(this.zDown){
            this.playerSpeed = -300;
            cc.log("z");
        }
        else if(this.xDown){
            cc.log("x");
            this.playerSpeed = 300;
        }
        else
            this.playerSpeed = 0;
        if(!this.isDead&&this.kDown&&this.onGround)
            this.jump();
        this.node.x += this.playerSpeed * dt;  //move player
    }

    //give velocity to the player
    private jump()
    {
        this.onGround = false;
        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0,300);
    }

    //check if the collision is valid or not
    onBeginContact(contact, selfCollider, otherCollider)
    {
        if(otherCollider.tag == 2 && !this.isDead){ //enemy tag
            this.isDead = true;
            cc.log("dead");
        }
        else if(otherCollider.tag == 1){
            cc.log("ground");
            this.onGround = true;
        }
    }
}